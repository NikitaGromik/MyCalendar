@extends('layouts.app')

@section('content')

    <div class="container">
        @include('errors')

        @if(Auth::check())

        <form action="{{ url('/task') }}" method="POST" class="form-group">
            {{ csrf_field() }}

          {{--  <div class="form-group">
                <label for="Task" class="col-sm-3 control-label">Task</label>
            </div>--}}
            <label for="title-name">Title of task:</label>
            <div class="row">
                <div class="col-sm-10">
                    <input autocomplete="disabled" type="text" name="title" id="title-name" class="form-control">
                </div>
                <div class="col-sm-2 text-center">
                    <button type="submit" class="btn btn-success">
                        Add Task
                    </button>
                </div>
            </div>
        </form>

        <div class="panel panel-default">
            @if(count($tasks) > 0)
            <div class="panel-heading">
                <div class="row table-container">
                    <div class="col-sm-6 col-table-cell">
                        <h4>The challenges in the coming month:</h4>
                    </div>
                    <div class="col-sm-6 text-right col-table-cell">
                        <a href="/">Current</a> |
                        <a href="{{url('/tasks/1')}}">Overdue</a> |
                        <a href="{{url('/tasks/2')}}">Completed</a> |
                        <a href="{{url('/tasks/3')}}">Date...</a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            @if($task->isFinished)
                                <td class="col-sm-11 bg-gray">
                            @else
                                <td class="col-sm-11">
                            @endif
                                <div>{{$task->title}}</div>
                            </td>
                                @if($task->isFinished)
                                    <td class="col-sm-1 bg-gray">
                                @else
                                    <td class="col-sm-1">
                                        @endif
                                <form action="{{url('view/' . $task->id)}}" method="GET">
                                    <button class="btn btn-info">
                                        More
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @else
                <div class="panel-heading">
                    <h4>The challenges in the coming month:</h4>
                </div>
                <div class="panel-body">You have not any tasks yet. <a href="/tasks">Show all tasks.</a></div>
            @endif
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Set a detailed task:</h4>
            </div>

            <div class="panel-body">
                <form id="taskForm" action="{{ url('/dtask') }}" method="POST" class="form-group">
                    {{ csrf_field() }}

                    {{--  <div class="form-group">
                          <label for="Task" class="col-sm-3 control-label">Task</label>
                      </div>--}}
                    <div>
                        <label for="title-name">Title of task:</label>
                        <input autocomplete="disabled" type="text" name="title" id="title-name" class="form-control">
                    </div>

                    <div class="top10">
                        <label for="type-name">Type of meeting:</label>
                        <select class="form-control" id="type-name" name="type">
                            <option value="M">Meeting</option>
                            <option value="D">Case</option>
                            <option value="C">Call</option>
                            <option value="N">Conference</option>
                        </select>
                    </div>

                    <div class="top10">
                        <label for="title-name">Venue:</label>
                        <input autocomplete="disabled" type="text" name="place" id="palce-name" class="form-control">
                    </div>

                    <div class="row top10">
                        <div class="col-sm-6">
                            <label for="title-name">Date:</label>
                            <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-start="1">
                                <input type="text" class="form-control" name="date">
                                <div class="input-group-addon">
                                    Click
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="time">Time:</label>
                            <select class="form-control" id="time" name="time">
                                <option value="7:00">7:00</option>
                                <option value="8:00">8:00</option>
                                <option value="9:00">9:00</option>
                                <option value="10:00">10:00</option>
                                <option value="11:00">11:00</option>
                                <option value="12:00">12:00</option>
                                <option value="13:00">13:00</option>
                                <option value="14:00">14:00</option>
                                <option value="15:00">15:00</option>
                                <option value="16:00">16:00</option>
                                <option value="17:00">17:00</option>
                                <option value="18:00">18:00</option>
                                <option value="19:00">19:00</option>
                                <option value="20:00">20:00</option>
                            </select>
                        </div>
                    </div>

                    <div class="top10">
                        <label for="duration">Duration:</label>
                        <select class="form-control" id="duration" name="duration">
                            <option value="15m">15 minutes</option>
                            <option value="30m">30 minutes</option>
                            <option value="1h">1 hour</option>
                            <option value="2h">2 hours</option>
                            <option value="3h">3 hours</option>
                            <option value="4h">4 hours</option>
                            <option value="5h">5 hours</option>
                        </select>
                    </div>

                    <div class="top10">
                        <label for="description">Description:</label>
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>

                    <div class="row top10">
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-default" onclick="clearForm()">
                                Clear
                            </button>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button type="submit" class="btn btn-success">
                                Add Task
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        @else

        <div class="alert alert-info">
            <a href="/login">You must be logged in.</a>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Advertisements:</h4>
            </div>
            <div class="panel-body">
                <h3>Case Opener 2018</h3>
                <div class="row">
                    <div class="col-sm-12 no-padding">
                        <img src="{{asset('images/BannerVKCO18.png')}}" class="img-responsive"/>
                    </div>
                </div>
                <div class="row top10">
                    <div class="col-sm-12">
                        Do you want to try your luck by opening real cases in CS: GO? Can you first test your luck in the simulator? Case Opener 2018 is the logical continuation of Case Opener 2017.

                        Here you will not spend anything! In addition, you have the opportunity to find out if your luck is enough to knock out a knife?
                    </div>
                </div>
                <a href="https://play.google.com/store/apps/details?id=com.nikkorejz.co18">Download for Android</a>

                <h3>Forlabs Schedule</h3>
                <div class="row">
                    <div class="col-sm-12 no-padding">
                        <img src="{{asset('images/fsima.png')}}" class="img-responsive"/>
                    </div>
                </div>
                <div class="row top10">
                    <div class="col-sm-12">
                        With this application, you can watch the schedule of your group without authorization. First select your group, and then the application will remember your group, and will show the schedule for today even without an Internet connection!

                        Actual only for students of the Faculty of Service and Advertising of Irkutsk State University
                    </div>
                </div>
                <a href="https://play.google.com/store/apps/details?id=ru.trixiean.forlabsschedule">Download for Android</a>
            </div>
        </div>

        @endif
    </div>

@endsection