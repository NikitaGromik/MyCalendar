@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            @if($task->isFinished)
                <div class="panel-heading panel-heading-custom">
            @else
                <div class="panel-heading">
            @endif
            <div class="row">
                <div class="col-sm-10">
                    <h4>{{$task->title}}</h4>
                </div>
                <div class="col-sm-2 text-right">
                    @if(!$isEditMode && !$task->isFinished)
                        <form action="{{ url('/view/' . $task->id) }}" method="POST" class="form-group">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-primary" name="edit" value="0">Edit</button>
                        </form>
                    @elseif(!$task->isFinished)
                        <form action="{{ url('/view/' . $task->id) }}" method="GET" class="form-group">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-default" name="edit" value="0">Cancel</button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
        <div class="panel-body">
            <form action="{{ url('/change/' . $task->id) }}" method="POST" class="form-group">
                {{ csrf_field() }}
                <div class="row">
                    @if(!$task->isFinished && !$isEditMode)
                        <div class="col-sm-10">
                            <h5>This task is in progress.</h5>
                        </div>
                        <div class="col-sm-2 text-right">
                            <button type="submit" class="btn btn-success" name="isFinished" value="1">Finished</button>
                        </div>
                    @elseif(!$isEditMode)
                        <div class="col-sm-10">
                            <h5>Cool, this task was accomplished!</h5>
                        </div>
                        <div class="col-sm-2 text-right">
                            <button type="submit" class="btn btn-default" name="isFinished" value="0">Task not completed</button>
                        </div>
                    @endif
                </div>
            </form>

            @if($isEditMode)
            <form action="{{url('task/save/' . $task->id)}}" method="POST">
                {{csrf_field()}}

            <div class="row">
                <div class="col-sm-12">
                    <label for="title">Title:</label>
                    <input class="form-control" id="title" name="title" value="{{$task->title}}"/>
                </div>
            </div>
            @endif

            {{-- Show description --}}
            <div class="row">
                <div class="col-sm-12">
                    <label for="description">Description:</label>
                    <textarea class="form-control" id="description" name="description" {{$isEditMode ? '' : 'disabled'}}>{{$task->description}}</textarea>
                </div>
            </div>

            @if(!$isEditMode)
                {{-- Show type and datetime + duration --}}
                <div class="row top10">
                    <div class="col-sm-2">
                        <label for="type">Type:</label><br />
                        @if($task->type === 'D')
                            <button class="btn btn-info" id="type">Deal</button>
                        @elseif($task->type === 'M')
                            <button class="btn btn-info" id="type">Meeting</button>
                        @elseif($task->type === 'C')
                            <button class="btn btn-info" id="type">Call</button>
                        @elseif($task->type === 'N')
                            <button class="btn btn-info" id="type">Conference</button>
                        @endif
                    </div>
                    <div class="col-sm-10">
                        <label for="datetime">Date & Time:</label>
                        <div class="input-group">
                            <input disabled class="form-control" id="datetime" value="{{$task->date}}"/>
                            <div class="input-group-addon">
                                ~{{$task->duration}}
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Show place --}}
                <div class="top10">
                    <label for="place">Venue:</label>
                    <input disabled class="form-control" id="place" value="{{$task->place}}"/>
                </div>
            @else
                <div class="top10">
                    <label for="type-name">Type of meeting:</label>
                    <select class="form-control" id="type-name" name="type">
                        <option value="M" {{$task->type === 'M' ? 'selected' : ''}}>Meeting</option>
                        <option value="D" {{$task->type === 'D' ? 'selected' : ''}}>Case</option>
                        <option value="C" {{$task->type === 'C' ? 'selected' : ''}}>Call</option>
                        <option value="N" {{$task->type === 'N' ? 'selected' : ''}}>Conference</option>
                    </select>
                </div>

                <div class="top10">
                    <label for="title-name">Venue:</label>
                    <input autocomplete="disabled" type="text" name="place" id="palce-name" class="form-control" value="{{$task->place}}" />
                </div>

                <div class="row top10">
                    <div class="col-sm-6">
                        <label for="title-name">Date:</label>
                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-start="1">
                            <input type="text" class="form-control" name="date" value="{{substr($task->date, 0, 10)}}">
                            <div class="input-group-addon">
                                Click
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label for="time">Time:</label>
                        <select class="form-control" id="time" name="time">
                            <option value="7:00">7:00</option>
                            <option value="8:00">8:00</option>
                            <option value="9:00">9:00</option>
                            <option value="10:00">10:00</option>
                            <option value="11:00">11:00</option>
                            <option value="12:00">12:00</option>
                            <option value="13:00">13:00</option>
                            <option value="14:00">14:00</option>
                            <option value="15:00">15:00</option>
                            <option value="16:00">16:00</option>
                            <option value="17:00">17:00</option>
                            <option value="18:00">18:00</option>
                            <option value="19:00">19:00</option>
                            <option value="20:00">20:00</option>
                        </select>
                    </div>
                </div>

                <div class="top10">
                    <label for="duration">Duration:</label>
                    <select class="form-control" id="duration" name="duration">
                        <option value="15m" {{$task->duration === '15m' ? 'selected' : ''}}>15 minutes</option>
                        <option value="30m" {{$task->duration === '30m' ? 'selected' : ''}}>30 minutes</option>
                        <option value="1h" {{$task->duration === '1h' ? 'selected' : ''}}>1 hour</option>
                        <option value="2h" {{$task->duration === '2h' ? 'selected' : ''}}>2 hours</option>
                        <option value="3h" {{$task->duration === '3h' ? 'selected' : ''}}>3 hours</option>
                        <option value="4h" {{$task->duration === '4h' ? 'selected' : ''}}>4 hours</option>
                        <option value="5h" {{$task->duration === '5h' ? 'selected' : ''}}>5 hours</option>
                    </select>
                </div>

                {{-- Save button --}}
                <hr />
                <div class="row">
                    <div class="col-sm-11">
                        <h5><b>Save changes?</b></h5>
                    </div>
                    <div class="col-sm-1 text-right">

                            <button type="submit" class="btn btn-success">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            @endif

            <hr />
            <div class="row">
                <div class="col-sm-11">
                    <h5><b>Delete this task?</b></h5>
                </div>
                <div class="col-sm-1 text-right">
                    <form action="{{url('task/delete/' . $task->id)}}" method="POST">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}

                        <button class="btn btn-danger">
                            Delete
                        </button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>

@endsection